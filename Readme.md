# Emotion Detection

## 參考來源
* https://github.com/atulapra/Emotion-detection

## 環境需求
* Python 3 (64位元)

## 安裝虛擬環境、套件
進入專案資料夾內，創建名叫「venv」的虛擬環境。
```
$ cd Emotion Detection
$ virtualenv venv
```

啟動虛擬環境
```
$ cd venv/Scripts
$ activate
```

cd 回去上上層，安裝套件
```
$ cd ..
$ cd ..
$ pip install -r requirements.txt
```

## 執行專案
* windows版
```
$ cd src
$ python display_windows.py --name {資料夾路徑}   #省略符號
```
* Linux版
```
$ python display_linux.py --name {資料夾路徑}     #省略符號
```