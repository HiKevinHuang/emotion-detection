import numpy as np
import cv2
from tensorflow.keras import models
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import os
from os import walk
import argparse
import win32file
import win32con
import time
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

target_folder = ""


def receive_argument():
    # command line argument
    ap = argparse.ArgumentParser()
    ap.add_argument_group('name', '輸入資料夾名稱')
    ap.add_argument("--name",help="type folder name")
    name = ap.parse_args().name
    return name


# 移除資料夾內的檔案
def remove_file():
    file_path = target_folder
    for root, dirs, files in os.walk(file_path):
        for name in files:
            os.remove(os.path.join(root, name))


# 偵測資料夾內的變動
def detect_folder_change():
    ACTIONS = {
    1 : "Created",
    2 : "Deleted",
    3 : "Updated",
    4 : "Renamed from something",
    5 : "Renamed to something"
    }
    # Thanks to Claudio Grondi for the correct set of numbers
    FILE_LIST_DIRECTORY = 0x0001
    path_to_watch = target_folder
    hDir = win32file.CreateFile (
    path_to_watch,
    FILE_LIST_DIRECTORY,
    win32con.FILE_SHARE_READ | win32con.FILE_SHARE_WRITE,
    None,
    win32con.OPEN_EXISTING,
    win32con.FILE_FLAG_BACKUP_SEMANTICS,
    None
    )
    while 1:
        results = win32file.ReadDirectoryChangesW (
            hDir,
            1024,
            True,
            win32con.FILE_NOTIFY_CHANGE_FILE_NAME |
            win32con.FILE_NOTIFY_CHANGE_DIR_NAME |
            win32con.FILE_NOTIFY_CHANGE_ATTRIBUTES |
            win32con.FILE_NOTIFY_CHANGE_SIZE |
            win32con.FILE_NOTIFY_CHANGE_LAST_WRITE |
            win32con.FILE_NOTIFY_CHANGE_SECURITY,
            None,
            None
        )

        for action, file in results:
            full_filename = os.path.join (path_to_watch, file)
            print (full_filename, ACTIONS.get (action, "Unknown"))
            if ACTIONS.get (action, "Unknown") == "Created":
                cv2.destroyAllWindows()
                recognize(full_filename)


# 表情辨識
def recognize(full_filename):
    # Create the model
    # tic = time.clock()
    model = Sequential()

    model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(48,48,1)))
    model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(7, activation='softmax'))


    # prevents openCL usage and unnecessary logging messages
    cv2.ocl.setUseOpenCL(False)

    # dictionary which assigns each label an emotion (alphabetical order)
    emotion_dict = {0: "Angry", 1: "Disgusted", 2: "Fearful", 3: "Happy", 4: "Neutral", 5: "Sad", 6: "Surprised"}

    model.load_weights('model.h5')

    frame = cv2.imread(full_filename)
    facecasc = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = facecasc.detectMultiScale(gray,scaleFactor=1.3, minNeighbors=5)

    # draw
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y-50), (x+w, y+h+10), (255, 0, 0), 2)
        roi_gray = gray[y:y + h, x:x + w]
        cropped_img = np.expand_dims(np.expand_dims(cv2.resize(roi_gray, (48, 48)), -1), 0)
        prediction = model.predict(cropped_img)
        maxindex = int(np.argmax(prediction))
        print(emotion_dict[maxindex])
        cv2.putText(frame, emotion_dict[maxindex], (x+20, y-60), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
    # toc = time.clock()
    # print(toc - tic)
    cv2.imshow('Video', cv2.resize(frame,(640,480),interpolation = cv2.INTER_CUBIC))
    cv2.waitKey(1000)
    # while True:
    #     res = cv2.waitKey(0)
    #     if res == 108: # enter
    #         print("ENTER")
    #         break
    #     if res == 65: # a
    #         print("A")
    #         break
    #     if res == 27: # esc
    #         print("ESC")
    #         break
    #     if res == 113: # q
    #         print("Q")
    #         cv2.destroyAllWindows()
    #     break

if __name__ == "__main__":
    folder_name = receive_argument()
    target_folder = folder_name
    try:
        detect_folder_change()
    except KeyboardInterrupt:
        cv2.destroyAllWindows()
        print("Stop !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")