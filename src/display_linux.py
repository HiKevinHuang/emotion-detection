import numpy as np
import cv2
from tensorflow.keras import models
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import os, sys
from os import walk
import json
import argparse
import inotify.adapters
import time
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# 資料夾名稱
full_folder_path = ""
folder_name = ""

# 接收CMD參數
def receive_argument():
    # command line argument
    ap = argparse.ArgumentParser()
    ap.add_argument_group('name', '輸入資料夾路徑、名稱')
    ap.add_argument("--name",help="type folder name")
    name = ap.parse_args().name
    
    # 檢查目錄是否存在 
    if os.path.isdir(name):
        print("目錄存在。")
        return name
    else:
        print("目錄不存在。")
        sys.exit()


# 檔案過多時，移除資料夾內的檔案
def remove_file():
    file_path = full_folder_path
    for root, dirs, files in os.walk(file_path):
        if len(files) > 100:
            for name in files:
                os.remove(os.path.join(root, name))


# 偵測資料夾內的變動(Ubuntu)
def detect_folder_change():
    global folder_name
    print("Folder path is : " + full_folder_path)
    i = inotify.adapters.Inotify()

    i.add_watch(full_folder_path)

    for event in i.event_gen(yield_nones=False):
        (_, type_names, path, filename) = event
        
        if type_names[0] == 'IN_CREATE':
            print("PATH=[{}] FILENAME=[{}] EVENT_TYPES={}".format(path, filename, type_names))
            cv2.destroyAllWindows()
            folder_name = os.path.split(path)[1]
            image = path + "/" + filename
            print("Folder name : " + str(folder_name))
            print("Image : " + str(image))
            recognize(image)
        # else:
        #     print("PATH=[{}] FILENAME=[{}] EVENT_TYPES={}".format(path, filename, type_names))


# 表情辨識
def recognize(full_filename):
    # Create the model
    model = Sequential()

    model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(48,48,1)))
    model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(7, activation='softmax'))


    # prevents openCL usage and unnecessary logging messages
    cv2.ocl.setUseOpenCL(False)

    # dictionary which assigns each label an emotion (alphabetical order)
    emotion_dict = {0: "Angry", 1: "Disgusted", 2: "Fearful", 3: "Happy", 4: "Neutral", 5: "Sad", 6: "Surprised"}

    model.load_weights('model.h5')

    frame = cv2.imread(full_filename)
    facecasc = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = facecasc.detectMultiScale(gray,scaleFactor=1.3, minNeighbors=5)

    emotion = []
    # draw
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y-50), (x+w, y+h+10), (255, 0, 0), 2)
        roi_gray = gray[y:y + h, x:x + w]
        cropped_img = np.expand_dims(np.expand_dims(cv2.resize(roi_gray, (48, 48)), -1), 0)
        prediction = model.predict(cropped_img)
        maxindex = int(np.argmax(prediction))
        print(emotion_dict[maxindex])
        emotion.append(emotion_dict[maxindex])
        cv2.putText(frame, emotion_dict[maxindex], (x+20, y-60), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
    # cv2.imshow('Video', cv2.resize(frame,(640,480),interpolation = cv2.INTER_CUBIC))
    # cv2.waitKey(1000)
    output_json(len(faces), emotion)


def output_json(faces, emotion):
    name = str(time.strftime("%m%d%H%M%S", time.localtime())) + ".json"
    data = {"room id":folder_name, "people_total":str(faces), "emotion":emotion}
    with open(name, 'w') as f:
        f.write(json.dumps(data, indent=4, sort_keys=True))
    
    remove_file()


if __name__ == "__main__":
    folder_path = receive_argument()
    full_folder_path = folder_path
    try:
        detect_folder_change()
    except KeyboardInterrupt:
        cv2.destroyAllWindows()
        print("Stop !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")